class CreateArtistsLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :artists_locations do |t|
      t.references :artist, foreign_key: true
      t.references :location, foreign_key: true

      t.timestamps
    end
  end
end
