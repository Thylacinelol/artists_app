class CreateArtistsTags < ActiveRecord::Migration[5.1]
  def change
    create_table :artists_tags do |t|
      t.references :artist, foreign_key: true
      t.references :tag, foreign_key: true

      t.timestamps
    end
  end
end
