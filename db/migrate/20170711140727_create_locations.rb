class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|
      t.jsonb :names

      t.timestamps
    end
  end
end
