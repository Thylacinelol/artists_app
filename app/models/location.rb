class Location < ApplicationRecord
  has_and_belongs_to_many :artists

  def name
    names['name_en']
  end
end
