class Artist < ApplicationRecord
  paginates_per 10

  has_and_belongs_to_many :locations
  has_and_belongs_to_many :tags

  def self.search(query)
    return Artist.all if query.blank?

    artist_ids = []
    tags = query.split(' ')
    matched_tags = []

    tags.each do |tag_name|
      matched_tags += Tag.where('names @> ?', { name_en: tag_name }.to_json).map(&:id)
    end

    Artist.includes(:tags).where(tags: { id: matched_tags })
  end
end
