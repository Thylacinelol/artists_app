class ArtistsTag < ApplicationRecord
  belongs_to :artist
  belongs_to :tag
end
