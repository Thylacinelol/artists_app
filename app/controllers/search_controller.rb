class SearchController < ApplicationController
  # GET /search
  def index
    @artists = Artist.search(params[:query])
    @artists = @artists.page(params[:page])
  end
end
