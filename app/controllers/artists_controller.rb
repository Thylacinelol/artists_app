class ArtistsController < ApplicationController
  before_action :set_artist, only: [:show, :edit, :update, :destroy]

  # GET /artists
  # GET /artists.json
  def index
    @artists = Artist.all
  end

  # GET /artists/1
  # GET /artists/1.json
  def show
    @artist_description = @artist.descriptions['description_en']

    @artist_location = 'Not specified'
    if @artist.locations.any?
      @artist_location = @artist.locations.first.names['name_en']
    end
  end

  # GET /artists/new
  def new
    @artist = Artist.new
    @tags = Tag.all
    @locations = Location.all
  end

  # GET /artists/1/edit
  def edit
    @tags = Tag.all
    @locations = Location.all
  end

  # POST /artists
  # POST /artists.json
  def create
    @artist = Artist.new
    use_some_form_object_to_do_this_instead(@artist)

    respond_to do |format|
      if @artist.save
        format.html { redirect_to @artist, notice: 'Artist was successfully created.' }
        format.json { render :show, status: :created, location: @artist }
      else
        format.html { render :new }
        format.json { render json: @artist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /artists/1
  # PATCH/PUT /artists/1.json
  def update
    @artist.tags.clear
    @artist.locations.clear
    use_some_form_object_to_do_this_instead(@artist)

    respond_to do |format|
      if @artist.save
        format.html { redirect_to @artist, notice: 'Artist was successfully updated.' }
        format.json { render :show, status: :ok, location: @artist }
      else
        format.html { render :edit }
        format.json { render json: @artist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /artists/1
  # DELETE /artists/1.json
  def destroy
    @artist.destroy
    respond_to do |format|
      format.html { redirect_to artists_url, notice: 'Artist was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_artist
      @artist = Artist.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def artist_params
      params.require(:artist).permit(:name, :tags, :locations, :description_en, :description_de)
    end

    def use_some_form_object_to_do_this_instead(record)
      record.name = artist_params[:name]
      record.descriptions = {}
      record.descriptions['description_en'] = artist_params['description_en']
      record.descriptions['description_de'] = artist_params['description_de']
      record.tags << Tag.where(id: params[:artist][:tags])
      record.locations << Location.where(id: params[:artist][:locations])
    end
end
